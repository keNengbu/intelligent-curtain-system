// pages/fish_tanks/fish_tanks.js

var mqtt = require('../../utils/mqtt.js')
var client = null

Page({

  /**
   * 页面的初始数据
   */
  data: {
    time: '12:00',
    temp:0.0,
    tds:"",
    level:"",
    pubTopic: 'XingHuan/curtain_kz'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.pubMsg();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  bindTimeChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      time: e.detail.value
    })
    client.publish("/lxy086/feedtime", e.detail.value)
  },
  feed(){
    client.publish("XingHuan/curtain_kz", '0')
  },
  feed1(){
    client.publish("XingHuan/curtain_kz", '1')
  },
  feed2(){
    client.publish("XingHuan/curtain_kz", '2')
  },
  pubMsg() {
    var that = this
    // 连接选项
    const options = {
      connectTimeout: 4000, // 超时时间
      // 认证信息 按自己需求填写
      clientId: 'YX_2_id'+ Math.ceil(Math.random()*10),
      port: '8083',
      username: 'iot',//服务器用户名
      password: 'iot123456',//服务器密码
    }

    client = mqtt.connect('wx://111.230.201.180/mqtt', options)

    client.on('connect', (e) => {
      console.log('服务器连接成功')
      client.subscribe('XingHuan/#', {
        qos: 0
      }, function (err) {
        if (!err) {
          console.log('订阅成功')
          client.publish(that.data.pubTopic, '小程序成功连接Mqtt')
        }
      })
    })
    client.on('message', function (topic, message) {
      console.log('收到来自主题 ' + topic + ' 的消息：' + message.toString())
      if (topic === 'XingHuan/curtain_ld') {
        // 处理 temp 主题的消息
        that.setData({
          temp:message.toString(),
        })
      } else if (topic === 'XingHuan/curtain_ydz') {
        // 处理 tds 主题的消息
        // if (Number(message.toString()) < 300) {
        //   that.setData({
        //     tds: '优',
        //   })
        // } else if (Number(message.toString()) >= 300 && Number(message.toString()) < 600) {
        //   that.setData({
        //     tds: '良',
        //   })
        // } else if (Number(message.toString()) >= 600 && Number(message.toString()) < 900) {
        //   that.setData({
        //     tds: '中',
        //   })
        // } else if (Number(message.toString()) >= 100) {
        //   that.setData({
        //     tds: '差',
        //   })
        // }
        that.setData({
          tds:message.toString(),
        })
      }
    })
    client.on('reconnect', (error) => {
      console.log('正在重连', error)
    })
    client.on('error', (error) => {
      console.log('连接失败', error)
    })
  },
  pub() {
    var that = this
    client.publish(that.data.pubTopic, '1')
  },
})