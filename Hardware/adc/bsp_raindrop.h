#ifndef _BSP_RAINDROP_H__
#define	_BSP_RAINDROP_H__

#include "gd32f4xx.h"



#define BSP_RAINDROP_GPIO_RCU_AO       RCU_GPIOF
#define BSP_RAINDROP_GPIO_PORT_AO      GPIOF
#define BSP_RAINDROP_GPIO_PIN_AO       GPIO_PIN_8

#define BSP_LIGHT_GPIO_RCU_AO          RCU_GPIOF
#define BSP_LIGHT_GPIO_PORT_AO         GPIOF
#define BSP_LIGHT_GPIO_PIN_AO          GPIO_PIN_6

#define BSP_ADC_RCU                    RCU_ADC2
#define BSP_ADC                        ADC2
#define BSP_RAINDROP_ADC_CHANNEL       ADC_CHANNEL_6
#define BSP_LIGHT_ADC_CHANNEL          ADC_CHANNEL_4
 
#define RAIN_MAX     50   
 
#define LIGHT_MAX    60
#define LIGHT_MIN    10

void raindrop_and_light_gpio_config(void);
unsigned int get_raindrop_percentage_value(void);
unsigned int get_light_percentage_value(void);

#endif
