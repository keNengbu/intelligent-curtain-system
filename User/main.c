#include "gd32f4xx.h"
#include "systick.h"
#include "bsp_usart.h"
#include "stdio.h"
#include "BSP_hlkv20.h"
#include "bsp_stepper_motor.h"
#include "bsp_ir_receiver.h"
#include "bsp_raindrop.h"
#include "cJSON.h"


char mode_flag = 0; /* 0 自动模式，1 手动模式 */
cJSON *cjson_head=NULL;
char *str=NULL;


void auto_mode(void){

  	uint16_t rain_value = 0;
    uint16_t light_value = 0;
    rain_value = get_raindrop_percentage_value();   /* 获取雨滴值 */
    light_value = get_light_percentage_value();     /* 获取光照值 */
	
	  cjson_head=cJSON_CreateObject();
    cJSON_AddNumberToObject(cjson_head,"rain_value",rain_value);
    cJSON_AddNumberToObject(cjson_head,"light_value",light_value);
    str = cJSON_Print(cjson_head);
  printf("%s\n", str);
	cJSON_Delete(cjson_head);
    if( rain_value >= RAIN_MAX ){
        
        printf(" 检测到大量雨滴，打开窗帘 \r\n ");
       open_curtain();
        return ;        /* 结束函数，直接关闭窗帘 */
    }
    if( light_value >= LIGHT_MAX ){
       
			
        printf("检测到光照太强，打开窗帘\r\n ");
       open_curtain();
    }
    if( light_value <= LIGHT_MIN ){
        
			
        printf("检测到光照太弱，关闭窗帘\r\n ");
        clear_curtain();
    }
		if( light_value <=LIGHT_MAX && light_value>=  LIGHT_MIN){
		printf("维持当前状态\r\n ");
		}
			free(str);

}

int main(void)
{
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);  //优先级分组
    systick_config();                //滴答定时器初始化 1ms

    usart_gpio_config(9600U);
    printf("start\r\n"); 
	
	HLK_USART_Init(9600);             //语音模块初始化
	Anakysis_Data();                  //解析数据
    
    raindrop_and_light_gpio_config();       /* 雨滴与光照检测初始化 */
    
    stepper_motor_config();                 /* 步进电机初始化  */ 
    stepper_motor_timer_config();           /* 步进电机转动频率设置 */ 
    
    motor_rest();
    
    infrared_goio_config();
	
    
	while(1)
	{
        if( mode_flag == 0){
            
            auto_mode();                    /* 自动模式 */
        }
        Anakysis_Data();                    /* 语音处理 */
        infrared_command_control();         /* 红外接收控制 */
        motor_stop_judgment();              /* 电机停止控制 */
				json_control();

	
        delay_1ms(200);
	}
}
